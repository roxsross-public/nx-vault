# by roxsross

Documentación Recomendada:

Installing Vault:
https://developer.hashicorp.com/vault/docs/install

Conceptos:
https://developer.hashicorp.com/vault/docs/concepts

Vault commands (CLI)
https://developer.hashicorp.com/vault/docs/commands

Secrets engines:
https://developer.hashicorp.com/vault/docs/secrets

Agent sidecar injector:
https://developer.hashicorp.com/vault/docs/platform/k8s/injector

Tutorial:
https://developer.hashicorp.com/vault/tutorials/kubernetes/kubernetes-sidecar